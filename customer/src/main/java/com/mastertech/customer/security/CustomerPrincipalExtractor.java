package com.mastertech.customer.security;

import java.util.Map;

import com.mastertech.customer.model.Customer;

import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

public class CustomerPrincipalExtractor implements PrincipalExtractor{

    @Override
    public Object extractPrincipal(Map<String, Object> map) {
        Customer customer = new Customer();

        customer.setId((Integer) map.get("id"));
        customer.setName((String) map.get("name"));

        return customer;
    }

}