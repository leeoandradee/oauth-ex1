package com.mastertech.customer.controller;

import com.mastertech.customer.model.Customer;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @GetMapping("/{id}")
    public Customer create(@PathVariable int id, @AuthenticationPrincipal Customer customer) {
        return customer;
    }
    
}